## full_s5400-user 5.0 LRX21M 1438668476 release-keys
- Manufacturer: micromax
- Platform: mt6582
- Codename: Q345
- Brand: Micromax
- Flavor: user
- Release Version: 5.0
- Id: LRX21M
- Incremental: 1438668476
- Tags: release-keys
- CPU Abilist: armeabi-v7a,armeabi
- A/B Device: false
- Locale: undefined
- Screen Density: 320
- Fingerprint: Micromax/Q345/Q345:5.0/LRX21M/1430987790:user/release-keys
- OTA version: 
- Branch: full_s5400-user-5.0-LRX21M-1438668476-release-keys
- Repo: micromax_q345_dump_20487


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
